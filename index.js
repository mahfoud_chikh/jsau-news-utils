const categories = [
        'A la une',
        'Sport',
        'Politique',
        'Effets diverts',
        'Santé',
        'Technologie'
    ];

const checkTitle = (title) => {
    return title && title.length > 5
}

function checkDescription(description) {
    return description && description.length > 0
}

function checkContent(content) {
    return content && content.length > 0
}

function checkAuthor(author) {
    return author && author.length > 5
}

function checkUrlToImg(url) {
    var expression =
        /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    return url && url.match(regex) != null;
}

function checkDate(date) {
    // var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    // return date && date.match(dateformat) != null;
    return true;
}


function checkCategory(category) {
    return categories.includes(category)
}


const validate = (data) => {

    const validations = {
        title: checkTitle(data.title),
        description: checkDescription(data.description),
        content: checkContent(data.content),
        author: checkAuthor(data.author),
        urlToImg: checkUrlToImg(data.urlToImg),
        date: checkDate(data.date),
	    category: checkCategory(data.category)
    };

    const isValid = Object.values(validations).reduce((x, y) => x && y);

    if (!isValid) {
        return validations;
    } else {
        return null;
    }

}


// console.log(validate({
//     "id": 19,
//     "title": "Sint sapiente delen",
//     "description": "kjsff",
//     "content": "qsdod",
//     "author": "Magna alias sed dese",
//     "urlToImg": "https://www.cowukejalox.org",
//     "date": "2019-11-19 11:26",
//     "category" : "Santé"
// }));


module.exports = {
    validate,
    categories
};

